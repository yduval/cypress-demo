# ✅ Cypress demo


## 🏗 Installation
```bash
npm init
npm install cypress --save-dev
```

## 🔧 Configuration

package.json
```json
{
    "scripts": {
        "test:e2e": "node_modules/.bin/cypress open",
        "ci:e2e": "node_modules/.bin/cypress run"
    },
}
```

Open first with `npm run test:e2e`. Cypress generates the files he needs and add a full set of nice examples !

```bash
# Clean generated files we wont use in our project
rm -rf cypress/fixtures/* cypress/integration/*
```

## 🔨 Tools
* [Cypress](https://docs.cypress.io/guides/overview/why-cypress.html)
* [Visual Studio Code](https://code.visualstudio.com/):
    * command Developer: Toggle screencast mode 
    * [Presentation mode](https://marketplace.visualstudio.com/items?itemName=jspolancor.presentationmode)
    * [emojisense](https://marketplace.visualstudio.com/items?itemName=bierner.emojisense)
    * [Gitlab-workflow](https://marketplace.visualstudio.com/items?itemName=fatihacet.gitlab-workflow)
* [Gitlab](https://about.gitlab.com/)
    * [Gitlab CI](https://docs.gitlab.com/ee/ci/yaml/)
* [Gitmoji](https://gitmoji.carloscuesta.me/)