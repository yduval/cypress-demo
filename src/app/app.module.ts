import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import {
  MatButtonModule,
  MatIconModule,
  MatToolbarModule,
  MatDialogModule,
  MatInputModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatCardModule,
  MatSelectModule,
  MatRadioModule,
  MAT_DATE_LOCALE,
  MatAutocompleteModule
} from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GravatarModule } from '@infinitycube/gravatar';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './croissant/dashboard/dashboard.component';
import { CroissantService } from './croissant/services/croissant.service';
import { WeekComponent } from './croissant/week/week.component';
import { DayComponent } from './croissant/day/day.component';
import { WidgetsModule } from './core/widgets/widgets.module';
import { ServicesModule } from './core/services/services.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EditionDialogComponent } from './croissant/edition-dialog/edition-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    WeekComponent,
    DayComponent,
    EditionDialogComponent
  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatDialogModule,
    MatInputModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatCardModule,
    MatSelectModule,
    MatRadioModule,
    MatAutocompleteModule,
    WidgetsModule,
    ServicesModule,
    FormsModule,
    GravatarModule
  ],
  providers: [
    CroissantService,
    {provide: LOCALE_ID, useValue: 'fr-FR'},
    {provide: MAT_DATE_LOCALE, useValue: 'fr-FR'}
  ],
  entryComponents: [
    EditionDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
