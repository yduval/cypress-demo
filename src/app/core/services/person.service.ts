import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Person } from '../models/person';
import { Observable } from 'rxjs/Observable';

import { environment as env } from '../../../environments/environment';

@Injectable()
export class PersonService {
  persons: Person[] = [];

  constructor(
    private http: HttpClient
  ) {}

  list(): Observable<Person[]> {
    return this.http.get(`${env.apiUrl}/persons`)
      .map((data: any[]) => data.map(p => new Person(p)))
      .publishLast()
      .refCount()
      .do((persons: Person[]) => this.persons = persons);
  }

  findPersonById(id: string): Person {
    return this.persons.find(p => p.id === id);
  }
}
