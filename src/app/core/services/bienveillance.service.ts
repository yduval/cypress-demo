import { Injectable } from '@angular/core';

@Injectable()
export class BienveillanceService {
  TITLE_LIST = [
    'Formidable !',
    'Époustouflant !',
    'Magnifique !',
    'Fantastique !',
    'Merveilleux !',
    'Incroyable !',
    'Succulent !',
    'Miam !',
    'Succulent !',
    'Parfait !',
    'Épatant !',
    'Chouette !',
    'Admirable !',
    'Remarquable !',
    'Excellent !',
    'Sensationnel !',
    'Exquis !'
  ];

  constructor() {}

  comment(): string {
    return this.TITLE_LIST[Math.floor(Math.random() * this.TITLE_LIST.length)];
  }

}
