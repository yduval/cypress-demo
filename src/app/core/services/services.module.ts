import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { MyInterceptor } from './http-interceptor';
import { ErrorService } from './error.service';
import { PersonService } from './person.service';
import { BienveillanceService } from './bienveillance.service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    PersonService,
    BienveillanceService,
    ErrorService,
    { provide: HTTP_INTERCEPTORS, useClass: MyInterceptor, multi: true }
  ]
})
export class ServicesModule {}
