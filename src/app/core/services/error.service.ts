import { Injectable } from '@angular/core';

import { NotificationService, Notification, NotificationStatus } from '../widgets/notification';

@Injectable()
export class ErrorService {

  constructor(
    private notification: NotificationService
  ) {}

  handle(error) {
    let message;
    let body;

    console.error(error);

    if (error._body) {
      body = error.json();

      if (body.error_description) {
        message = body.error_description;
      } else if (body.errors) {
        message = body.errors[0].message;
      }
    }
    if (!message) {
      message = 'Check your console (F12 or ⌘⌥I)';
    }

    this.notification.pop(new Notification({
      title: error.statusText,
      message: message,
      status: NotificationStatus.ERROR
    }));
  }
}
