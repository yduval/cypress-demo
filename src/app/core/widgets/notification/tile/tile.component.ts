import { Component, OnInit, Input } from '@angular/core';

import { Notification, NotificationStatus, NotificationService } from '../.';

@Component({
  selector: 'app-tile',
  templateUrl: './tile.component.html',
  styleUrls: ['./tile.component.scss']
})
export class TileComponent implements OnInit {
  @Input('notification') notification: Notification;

  constructor(
    private service: NotificationService
  ) {}

  ngOnInit() {
    if (!this.notification.action1) {
      this.notification.action1 = {
        label: 'OK',
        callback: () => this.service.remove(this.notification)
      };
    }
  }

  getStatus() {
    return this.notification.status ? NotificationStatus[this.notification.status] : NotificationStatus[NotificationStatus.INFO];
  }
}
