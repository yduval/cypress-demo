import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material';

import { NotificationService } from '.';
import { TileContainerComponent } from './tile-container/tile-container.component';
import { TileComponent } from './tile/tile.component';

@NgModule({
  providers: [
    NotificationService
  ],
  imports: [
    CommonModule,
    MatButtonModule
  ],
  declarations: [
    TileContainerComponent,
    TileComponent
  ],
  exports: [
    TileContainerComponent
  ]
})
export class NotificationModule { }
