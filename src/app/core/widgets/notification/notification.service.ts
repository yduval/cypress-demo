import { Injectable } from '@angular/core';

import { Notification } from '.';

@Injectable()
export class NotificationService {
  notifications: Notification[] = [];

  constructor() { }

  pop(notif: Notification): void {
    this.notifications.push(notif);

    if (!notif.delay) {
      notif.delay = 10000;
    }

    setTimeout(
      () => {
        this.remove(notif);
      },
      notif.delay
    );
  }

  remove(notif: Notification): void {
    for (let i = 0; i < this.notifications.length; i++) {
      if (this.notifications[i].id === notif.id) {
        this.notifications.splice(i, 1);
      }
    }
  }
}
