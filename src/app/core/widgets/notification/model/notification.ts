export class Notification {
  id: string;
  title: string;
  message: string;
  status: NotificationStatus;
  action1: ActionType;
  action2: ActionType;
  delay: number;

  constructor(json) {
    this.id = Math.random().toString(36);
    this.title = json.title;
    this.message = json.message;
    this.status = json.status ? json.status : NotificationStatus.INFO;
    this.delay = json.delay;
    this.action1 = json.action1;
    this.action2 = json.action2;
  }
}

export class ActionType {
  label: string;
  callback: any;
}

export enum NotificationStatus {
  // bug : impossible to have correct value of NotificationStatus[0] hence this useless value
  VOID,
  // bug ?
  ERROR,
  INFO,
  SUCCESS,
}
