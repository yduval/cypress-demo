import { NotificationService } from './notification.service';
import { Notification, NotificationStatus } from './model/notification';

export {
  NotificationService,
  Notification,
  NotificationStatus
};
