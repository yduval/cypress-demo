import { Component, OnInit } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

import { NotificationService } from '../.';

@Component({
  selector: 'app-tile-container',
  templateUrl: './tile-container.component.html',
  styleUrls: ['./tile-container.component.scss'],
  animations: [
    trigger('slide-in', [
      state('in', style({transform: 'translateX(0)', opacity: 1})),
      transition(':enter', [
        style({
          transform: 'translateX(100%) scale(0.3)',
          opacity: 0,
        }),
        animate('.3s cubic-bezier(0.175, 0.885, 0.32, 1.275)')
      ]),
      transition(':leave', [
        animate(
          '.4s .2s cubic-bezier(0.47, 0, 0.745, 0.715)',
          style({
            height: 0,
            transform: 'translateX(100%) scale(0.3)'
          })
        )
      ])
    ])
  ]
})
export class TileContainerComponent implements OnInit {

  constructor(
    public service: NotificationService
  ) { }

  ngOnInit() {}

}
