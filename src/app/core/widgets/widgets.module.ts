import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationModule } from './notification/notification.module';

@NgModule({
  imports: [
    CommonModule,
    NotificationModule
  ],
  declarations: [],
  exports: [
    NotificationModule
  ]
})
export class WidgetsModule {}
