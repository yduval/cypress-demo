export class Person {
  id: string;
  lastName: string;
  firstName: string;
  email: string;

  constructor(json?) {
    if (!json) {
      return;
    }
    this.id = json.id;
    this.lastName = json.lastName;
    this.firstName = json.firstName;
    this.email = json.email;
  }
}
