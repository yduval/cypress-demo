import { Person } from '../../core/models/person';

export class Croissant {
  id: string;
  reason: string;
  person: Person;
  personId: string;
  date: Date;

  constructor(json?) {
    if (!json) {
      return;
    }
    this.id = json.id;
    this.reason = json.reason;
    this.personId = json.personId;
    this.date = json.date ? new Date(json.date) : undefined;
  }
}
