import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";

import { PersonService } from "../../core/services/person.service";
import { Croissant } from "../models/croissant";
import { environment as env } from "../../../environments/environment";
import * as moment from "moment";
import "rxjs/Rx";

@Injectable()
export class CroissantService {
  croissants: Croissant[];

  constructor(private http: HttpClient, private personService: PersonService) {}

  list(): Observable<Croissant[]> {

    return this.http
      .get(
        `${env.apiUrl}/croissants?fromDate=${moment()
          .subtract(1, "M")
          .format("DD-MM-YYYY")}`
      )
      .map((data: any[]) => data.map(c => new Croissant(c)))
      .map(croissants =>
        croissants.map(c => {
          c.person = this.personService.findPersonById(c.personId);
          return c;
        })
      )
      .publishLast()
      .refCount()
      .do(c => (this.croissants = c));
  }

  create(croissant: any): Observable<any> {
    return this.http
      .post(`${env.apiUrl}/croissant`, croissant)
      .do(() => this.list().subscribe());
  }

  update(id: string, croissant: any): Observable<any> {
    return this.http
      .put(`${env.apiUrl}/croissant/${id}`, croissant)
      .do(() => this.list().subscribe());
  }

  delete(id: string) {
    return this.http
      .delete(`${env.apiUrl}/croissant/${id}`, { responseType: "text" })
      .do(() => this.list().subscribe());
  }
}
