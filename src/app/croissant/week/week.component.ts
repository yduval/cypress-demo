import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';

import { Croissant } from '../models/croissant';

@Component({
  selector: '[app-week]',
  templateUrl: './week.component.html',
  styleUrls: ['./week.component.scss']
})
export class WeekComponent implements OnInit {
  @Input('index') index: number;
  @Input('croissants') croissants: Croissant[];
  public week: any;

  constructor() {}

  ngOnInit() {
    moment.locale('fr');
    this.week = moment().add(this.index, 'w').startOf('week');
  }

  getDayOfWeek(dayToAdd = 0): any {
    return moment(this.week).add(dayToAdd, 'd');
  }

  getCroissants(dayToAdd = 0): Croissant[] {
    if (!this.croissants) {
      return [];
    }
    return this.croissants.filter(c => {
      return moment(c.date).dayOfYear() === this.getDayOfWeek(dayToAdd).dayOfYear();
    })
  }
}
