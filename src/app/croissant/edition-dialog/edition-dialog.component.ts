import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { NgForm, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import * as moment from 'moment';

import { NotificationService, NotificationStatus, Notification } from '../../core/widgets/notification';
import { Croissant } from '../models/croissant';
import { CroissantService } from '../services/croissant.service';
import { PersonService } from '../../core/services/person.service';
import { ErrorService } from '../../core/services/error.service';
import { BienveillanceService } from '../../core/services/bienveillance.service';
import { Observable } from "rxjs";
import { Person } from "../../core/models/person";

@Component({
  selector: 'app-edition-dialog',
  templateUrl: './edition-dialog.component.html',
  styleUrls: ['./edition-dialog.component.scss']
})
export class EditionDialogComponent implements OnInit {
  personCtrl: FormControl;
  @ViewChild('form') form: NgForm;
  croissant: Croissant;
  time: string;
  isNew: boolean;
  isLoading = false;
  now = new Date();
  max = moment().add(3, 'w').toDate();
  filteredPersons: Observable<any[]>;
  weekFilter = (d: Date): boolean => {
    const day = d.getDay();
    // Prevent Saturday and Sunday from being selected.
    return day !== 0 && day !== 6;
  };

  constructor(
    private dialogRef: MatDialogRef<EditionDialogComponent>,
    private notification: NotificationService,
    private service: CroissantService,
    public personService: PersonService,
    private bienveillance: BienveillanceService,
    private errorService: ErrorService,
    @Inject(MAT_DIALOG_DATA) private data: any
  ) {
    this.personCtrl = new FormControl();
    this.filteredPersons = this.personCtrl.valueChanges
        .startWith(null)
        .map(search => search ? this.filterPersons(search) : this.personService.persons.slice());
  }

  ngOnInit() {
    this.croissant = this.data instanceof Croissant ? Object.assign({}, this.data) : new Croissant(this.data);
    this.isNew = this.croissant.id === undefined;
    this.time = (this.croissant.date && this.croissant.date.getHours() > 12 ) ? 'afternoon' : 'morning';
  }

  filterPersons(search): Person[] {
    if (search instanceof Person) {
      return [];
    }
    return this.personService.persons.filter(person => {
      return person.firstName.toLowerCase().indexOf(search.toLowerCase()) >= 0 ||
        person.lastName.toLowerCase().indexOf(search.toLowerCase()) >= 0 ||
        person.email.toLowerCase().indexOf(search.toLowerCase()) >= 0;
    });
  }

  delete(): void {
    this.isLoading = true;
    this.service
      .delete(this.croissant.id)
      .subscribe({
        next: () => this.onSuccess('Mais ça fait moins de croissant pour la communauté 😔'),
        error: err => this.onError(err)
      });
  }

  displayFn(person: Person): any {
    return person ? (person.firstName + ' ' + person.lastName) : person;
  }

  submit(values: any): void {
    if (this.form.invalid) {
      return;
    }

    const date = values.date;

    if (values.time == 'morning') {
      date.setHours(9);
      date.setMinutes(45);
    } else {
      date.setHours(17);
      date.setMinutes(0);
    }
    date.setSeconds(0);

    let data = {
      personId: this.personCtrl.value.id,
      reason: values.reason,
      timestamp: date.getTime() / 1000
    };

    this.isLoading = true;

    if (this.isNew) {
      this.service
        .create(data)
        .subscribe({
          next: () => this.onSuccess('Tes camarades seront notifiés de ta bienveillance à leur égard 👍'),
          error: err => this.onError(err)
        });
    } else {
      this.service
        .update(this.croissant.id, data)
        .subscribe({
          next: () => this.onSuccess('Tes modifications ont été prises en compte 😃'),
          error: err => this.onError(err)
        });
    }
  }

  onSuccess(message: string): void {
    this.isLoading = false;
    this.notification.pop(new Notification({
      title: this.bienveillance.comment(),
      message: message,
      status: NotificationStatus.SUCCESS
    }));
    this.dialogRef.close();
  }

  onError(err: any): void {
    this.isLoading = false;
    this.errorService.handle(err);
  }
}
