import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import * as moment from 'moment';

import { EditionDialogComponent } from '../edition-dialog/edition-dialog.component';
import { Croissant } from '../models/croissant';

@Component({
  selector: '[app-day]',
  templateUrl: './day.component.html',
  styleUrls: ['./day.component.scss']
})
export class DayComponent implements OnInit {
  @Input('day') day: any;
  @Input('croissants') croissants: Croissant[];
  filteredCroissants: Croissant[] = [];
  isDayPassed: boolean;

  constructor(
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.isDayPassed = this.day.isBefore(moment(), 'd');
  }

  showCroissantEditionDialog(day: any, croissant?: Croissant): void {
    const data = croissant ? croissant : { date: day.toDate() };
    console.log(data)

    const dialogRef = this.dialog.open(EditionDialogComponent, {
      width: '400px',
      data: data
    });

    dialogRef
      .afterClosed()
      .subscribe();
  }
}
