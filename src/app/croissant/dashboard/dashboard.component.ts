import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { CroissantService } from '../services/croissant.service';
import { NotificationService, NotificationStatus, Notification } from '../../core/widgets/notification';
import { EditionDialogComponent } from '../edition-dialog/edition-dialog.component';
import { PersonService } from "../../core/services/person.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(
    public service: CroissantService,
    private personService: PersonService,
    private notification: NotificationService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.loadPersons();
  }

  loadPersons() {
    this.personService
      .list()
      .subscribe(() => this.loadCroissants());
  }

  loadCroissants() {
      this.service
        .list()
        .subscribe();
  }

  showCroissantEditionDialog(): void {
    const dialogRef = this.dialog.open(EditionDialogComponent, {width: '400px'});

    dialogRef
      .afterClosed()
      .subscribe();
  }
}
