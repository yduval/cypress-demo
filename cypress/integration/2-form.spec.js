/// <reference types="Cypress" />

const monday = Cypress.moment()
  .day(1)
  .hour(8);

describe("2. 🥐 Croissant form", () => {
  beforeEach(() => {
    cy.server();
    cy.route("GET", "*/persons", "fixture:persons").as("fetchPersons");
    cy.route("GET", "*/croissants*", "fixture:croissants").as(
      "fetchCroissants"
    );

    // Set date to Monday of the current week
    cy.clock(monday.toDate().getTime());

    cy.visit("/");

    // Open the creation form
    cy.get("header button").click();
  });

  it("2.1 should show error messages when submitting with form empty", () => {
    // When submiting
    cy.get('[type="submit"]').click();

    // Error messages should show
    cy.get("mat-form-field.ng-invalid").should("have.length", 3);
    cy.contains("Un petit commentaire est requis").should("be.visible");
    cy.contains("Date invalide").should("be.visible");
    cy.contains("Se désigner est un plus non négociable").should("be.visible");
  });

  it("2.2 should hide the messages when filling the fields", () => {
    cy.get('[type="submit"]').click();

    cy.get("#mat-input-0").type("Commentaire");
    cy.contains("Un petit commentaire est requis").should("not.exist");

    cy.get(".mat-datepicker-toggle-default-icon").click();
    cy.get(".mat-calendar-body-today").click();
    cy.contains("Date invalide").should("not.exist");

    cy.get('[data-cy="person"] input')
      .click()
      .type("Cersei Lannister")
    cy.contains("Se désigner est un plus non négociable").should("not.exist");
  });

  it("2.3 should be able to submit, close the window and display a success notification", () => {
    cy.route("POST", "*/croissant", {}).as("submitRequest");
    const reason = "Commentaire";

    cy.get('[type="submit"]').click();
    cy.get("#mat-input-0").type(reason);
    cy.get(".mat-datepicker-toggle-default-icon").click();
    cy.get(".mat-calendar-body-today").click();
    cy.get('[data-cy="person"] input')
      .click()
      .type("Cersei Lannister");

    cy.get('[type="submit"]').click();

    // Wait also permits to make assertions on the request
    cy.wait("@submitRequest").then(xhr => {
      expect(xhr.request.body.reason).to.equal(reason);
      expect(Cypress.moment(xhr.request.body.timestamp * 1000).date()).to.equal(
        monday.date()
      );
    });

    cy.get("form").should("not.exist");
    cy.get(".notification.SUCCESS p").should(
      "have.text",
      "Tes camarades seront notifiés de ta bienveillance à leur égard 👍"
    );
  });

  it("2.4 should not close the form, display an error notification if network error", () => {
    cy.route({ method: "POST", status: 500, url: "*/croissant" }).as(
      "submitRequest"
    );

    cy.get('[type="submit"]').click();
    cy.get("#mat-input-0").type("Commentaire");
    cy.get(".mat-datepicker-toggle-default-icon").click();
    cy.get(".mat-calendar-body-today").click();
    cy.get('[data-cy="person"] input')
      .click()
      .type("Cersei Lannister");

    cy.get('[type="submit"]').click();

    cy.wait("@submitRequest");

    cy.get("form").should("be.visible");
    cy.get(".notification.ERROR h1").should("have.text", "Unknown Error");
  });
});
