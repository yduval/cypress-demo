/// <reference types="Cypress" />

const dayOfWeek = Cypress.moment()
  .weekday(1)
  .hour(8);

describe("1. 👋 Hello Idean", () => {
  beforeEach(() => {
    cy.clock(dayOfWeek.toDate().getTime());

    cy.server();
    cy.route("GET", "*/persons", "fixture:persons").as("fetchPersons");

    cy.fixture("croissants").then(croissants => {
      cy.route(
        "GET",
        "*/croissants*",
        croissants.map((c, i) => ({
          ...c,
          date: dayOfWeek.clone()
            .add(i, "days")
            .toDate()
        }))
      ).as("fetchCroissants");
    });
    cy.visit("/");
  });

  it.skip("should be able to visit the Idean website", () => {
    cy.visit("https://www.idean.com/");

    // Querying Elements
    cy.get("h2")
      .contains("Hack the design system")
      // assertion
      .should("be.visible");
  });

  it("1.1 should be able to visit the croissant board (5 columns, 4 rows)", () => {
    // We can chain assertions
    cy.get(".app-name")
      .should("have.text", "Croissant Board")
      .should("be.visible");

    cy.get("h1").contains("Agenda");

    cy.get("table th").should("have.length", 5);
    cy.get("table tbody td").should("have.length", 20);
  });

  it("1.2 should display today in first line", () => {
    const today = Cypress.moment();

    // Assertions return the previously queried element
    cy.get("body")
      .get("table tbody tr")
      .first()
      .should("be.visible")
      .contains(today.format("D MMM"))
      .should("exist");
  });

  it("1.3 should display no events by default", () => {
    cy.visit("/");
    cy.get(".item").should("not.exist");
  });

  it("1.4 should display croissants from server", () => {
    cy.wait("@fetchCroissants")
      .its("url")
      .should(
        "include",
        `?fromDate=${dayOfWeek.subtract(1, "M").format("DD-MM-YYYY")}`
      );
    cy.get(".item").should("have.length", 2);
  });
});
